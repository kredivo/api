const Customer = require('../models').customers;
const db = require('../models');

let response = {
  status: 'error',
  message: '',
  data: null,
}

let statusCode = 400

exports.all = async function(req, res, next) {
  try {
    const customers = await Customer.findAll({});
  
    if (customers) {
      response.status = 'success';
      response.data = customers;
      statusCode = 200;
      response.message = 'Customers fetched';
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.reporting = async function(req, res, next) {
  try {
    const customers = await db.sequelize.query("SELECT c.id 'customers_id', c.name, c.phone, i.id 'invoice_id', i.created_at, i.total FROM customers c LEFT OUTER JOIN (SELECT customer_id, max(id) as 'MaxID' FROM invoices GROUP BY customer_id) latest_invoices ON c.id = latest_invoices.customer_id LEFT OUTER JOIN invoices i ON latest_invoices.customer_id = i.customer_id AND latest_invoices.MaxID = i.id", {
      type: db.sequelize.QueryTypes.SELECT
    });

    if (customers) {
      response.status = 'success';
      response.data = customers;
      statusCode = 200;
      response.message = 'Customers fetched';
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}