const Invoice = require('../models').invoices;
const Item = require('../models').items;
const Customer = require('../models').customers;
const InvoiceItem = require('../models').invoice_items;
const { validationResult } = require('express-validator');

let response = {
  status: 'error',
  message: '',
  data: null,
}

let statusCode = 400

exports.all = async function(req, res, next) {
  try {
    const invoices = await Invoice.findAll({
      include: ["customer"]
    });
  
    if (invoices) {
      response.status = 'success';
      response.data = invoices;
      statusCode = 200;
      response.message = 'Invoices fetched';
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.show = async function(req, res, next) {
  try {
    const invoice = await Invoice.findOne({
      where: {
        id: req.params.id
      },
      include: ["items"]
    });
  
    if (invoice) {
      response.status = 'success';
      response.data = invoice;
      response.message = 'Invoice fetched';
      statusCode = 200
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.store = async function(req, res, next) {
  try {
    let errors = validationResult(req)

    if (!errors.isEmpty()) {
      response.message = errors.errors[0].msg
    } else {
      const final_items = []
      const item_failure = false
      let sub_total = 0
    
      // Fetch customer
      const customer = await Customer.findOne({
        where: {
          id: req.body.customer_id
        }
      })

      if (customer) {
        // Validate each items
        // Count its price
        await Promise.all(req.body.invoice_items.map(async item => {
          if (!item || !item.id) {
            item_failure = true;
            response.message = (item.name ? 'Item ' + item.name  + ' is invalid, please try again': 'An item you selected is invalid, please try again');
            return false;
          }
    
          if (item.quantity <= 0) {
            item_failure = true;
            response.message = 'One of the item has 0 or less quantity, please adjust to the right quantity';
            return false;
          }
    
          let fetchItem = await Item.findOne({
            where: {
              id: item.id
            }
          })
    
          if (!fetchItem) {
            item_failure = true;
            response.message = 'Item ' + item.name + ' not found, please try again';
            return false;
          }
    
          const amount = fetchItem.unit_price * item.quantity
    
          final_items.push({
            item: fetchItem,
            quantity: item.quantity,
            unit_price: fetchItem.unit_price,
            amount: amount 
          })
    
          sub_total += amount
        }))
    
        if (!item_failure && final_items.length > 0) {
          // Create Invoice
          const create = await Invoice.create({
            customer_id: customer.id,
            customer_name: customer.name,
            customer_phone: customer.phone,
            customer_address: customer.address,
            sub_total: sub_total,
            tax: sub_total * 0.1,
            total: sub_total * 1.1
          })
        
          if (create) {
            // Insert All Items
            await Promise.all(final_items.map(async item_object => {
              const item = item_object.item
              
              const insertInvoiceItem = await InvoiceItem.create({
                invoice_id: create.id,
                item_id: item.id,
                name: item.name,
                quantity: item_object.quantity,
                unit_price: item_object.unit_price,
                amount: item_object.amount
              })
            }));
    
            response.status = 'success';
            response.message = 'Invoice created';
            response.data = create;
            statusCode = 200
          } else {
            response.message = 'Failed to create';
          }
        } else {
          response.message = 'Item invalid'
        }
      } else {
        response.message = 'Customer not found'
      }
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.update = async function(req, res, next) {
  try {
    let errors = validationResult(req)

    if (!errors.isEmpty()) {
      response.message = errors.errors[0].msg
    } else {
      // Find previous Invoice
      const previousInvoice = await Invoice.findOne({
        where: {
          id: req.params.id
        }
      })
  
      if (previousInvoice) {
        let newCustomer = null
        // If editing want to also overwrite previous customer
        if (req.body.customer_id) {
          newCustomer = await Customer.findOne({
            where: {
              id: req.body.customer_id
            }
          })
  
          if (!newCustomer) {
            response.message = 'Customer does not exist'
            throw Error('Customer does not exist')
          }
        }
  
        const final_items = []
        const item_failure = false
        let sub_total = 0
        // Validate each items
        // Count its price
        await Promise.all(req.body.invoice_items.map(async item => {
          if (!item || !item.id) {
            item_failure = true;
            response.message = (item.name ? 'Item ' + item.name  + ' is invalid, please try again': 'An item you selected is invalid, please try again');
            return false;
          }
    
          if (item.quantity <= 0) {
            item_failure = true;
            response.message = 'One of the item has 0 or less quantity, please adjust to the right quantity';
            return false;
          }
    
          let fetchItem = await Item.findOne({
            where: {
              id: item.id
            }
          })
    
          if (!fetchItem) {
            item_failure = true;
            response.message = 'Item ' + item.name + ' not found, please try again';
            return false;
          }
    
          const amount = fetchItem.unit_price * item.quantity
    
          final_items.push({
            item: fetchItem,
            quantity: item.quantity,
            unit_price: fetchItem.unit_price,
            amount: amount 
          })
    
          sub_total += amount
        }));
    
        if (!item_failure && final_items.length > 0) {
          // Check for the exisiting items in invoice
          const existingInvoiceItems = await InvoiceItem.findOne({
            where: {
              invoice_id: previousInvoice.id
            }
          })
  
          if (existingInvoiceItems) {
            // Delete All Previous Items
            const deleteInvoiceItems = await InvoiceItem.destroy({
              where: {
                invoice_id: previousInvoice.id
              }
            })
  
            if (!deleteInvoiceItems) {
              response.message = 'Failed to delete previous invoice items'
              throw Error('Failed to delete items')
            }
          }
  
          // Insert All Items
          await Promise.all(final_items.map(async item_object => {
            const item = item_object.item
            
            const insertInvoiceItem = await InvoiceItem.create({
              invoice_id: previousInvoice.id,
              item_id: item.id,
              name: item.name,
              quantity: item_object.quantity,
              unit_price: item_object.unit_price,
              amount: item_object.amount
            })
          }));
  
          // Update Invoice
          const update = await Invoice.update({
            customer_id: newCustomer ? newCustomer.id : previousInvoice.customer_id,
            customer_name: newCustomer ? newCustomer.name : previousInvoice.customer_name,
            customer_phone: newCustomer ? newCustomer.phone : previousInvoice.customer_phone,
            customer_address: newCustomer ? newCustomer.address : previousInvoice.customer_address,
            sub_total: sub_total,
            tax: sub_total * 0.1,
            total: sub_total * 1.1
          }, {
            where: {
              id: previousInvoice.id
            }
          })
        
          if (update) {
            response.status = 'success';
            response.data = previousInvoice;
            response.message = 'Invoice updated';
            statusCode = 200;
          } else {
            response.message = 'Failed to update';
          }
        } else {
          response.message = 'Item invalid, please try again'
        }
      } else {
        response.message = 'Invoice not found'
      }
    }
  } catch (error) {
    response.message = response.message ? response.message : 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.destroy = async function(req, res, next) {
  try {
    const invoice = await Invoice.findOne({
      where: {
        id: req.params.id
      }
    });
  
    if (invoice) {
      const destroy_childrens = await InvoiceItem.destroy({
        where: {
          invoice_id: invoice.id
        }
      })
      
      const destroy_parent = await Invoice.destroy({
        where: {
          id: invoice.id
        }
      })

      if (destroy_parent) {
        response.status = 'success';
        response.message = 'Invoice deleted';
        statusCode = 200;
      } else {
        response.message = 'Failed to delete';
      }
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}
