const Item = require('../models').items;

let response = {
  status: 'error',
  message: '',
  data: null,
}

let statusCode = 400

exports.all = async function(req, res, next) {
  try {
    const items = await Item.findAll({});
  
    if (items) {
      response.status = 'success';
      response.data = items;
      response.message = 'Items fetched';
      statusCode = 200;
    } else {
      response.message = 'Failed to fetch';
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}