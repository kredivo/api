const User = require('../models').users;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwt');

let response = {
  status: 'error',
  message: '',
  data: null,
}

let statusCode = 400

exports.signIn = async function(req, res) {
  try {
    const user = await User.findOne({
      where: {
        email: req.body.email
      }
    })

    if (!user) {
      response.message = 'Email/password invalid';
    } else {
      const checkPassword = bcrypt.compareSync(req.body.password, user.password);

      if (!checkPassword) {
        this.message = 'Email/password invalid';
      } else {
        const token = jwt.sign({
          id: user.id,
          email: user.email,
          role: user.role
        }, jwtConfig.secret, {
          expiresIn: 86400 
        })

        response.status = 'success'
        response.message = 'Login successful'
        response.data = token
        statusCode = 200
      }
    }
    // })
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}

exports.fetch = async function(req, res) {
  try {
    const tokenHeader = req.headers['authorization'];

    if (tokenHeader.split(' ')[0] !== 'Bearer') {
      response.message = 'Incorrect token format';
    } else {
      const token = tokenHeader.split(' ')[1];

      if (!token) {
        response.message = 'Token required'
      } else {
        jwt.verify(token, jwtConfig.secret, (error, decoded) => {
          if (error) {
            response.message = error;
            statusCode = 500;
          } else {
            response.message = 'User fetched';
            response.status = 'success';
            response.data = decoded;
            statusCode = 200;
          }
        })
      }
    }
  } catch (error) {
    response.message = 'Server error, please try again';
    console.log(error);
  }

  res.status(statusCode).json(response);
}