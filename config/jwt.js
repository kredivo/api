require('dotenv').config()

module.exports = {
  'secret': process.env.SECRET,
  ROLES: ['staff', 'lead', 'director']
}