const express = require('express');
const router = express.Router();
const InvoiceController = require('../controllers/InvoiceController');
const { body } = require('express-validator');

// All Invoices
router.get('/', InvoiceController.all);

// Show an Invoice
router.get('/:id', InvoiceController.show);

// Create New Invoice
router.post('/', [
  body('customer_id', 'Customer ID required').exists().not().isEmpty(),
  body('invoice_items', 'Invoice items required').exists().not().isEmpty()
], InvoiceController.store);

// Update Invoice
router.put('/:id', [
  body('invoice_items', 'Invoice items required').exists().not().isEmpty()
], InvoiceController.update);

// Destroy Invoice
router.delete('/:id', InvoiceController.destroy);

module.exports = router;
