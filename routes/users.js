const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');

// Sign In
router.post('/sign-in', UserController.signIn);

// Fetch
router.get('/fetch', UserController.fetch);

module.exports = router;
