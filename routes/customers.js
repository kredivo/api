const express = require('express');
const router = express.Router();
const CustomerController = require('../controllers/CustomerController');

// All Customers
router.get('/', CustomerController.all);

// Get Reporting - All Customers with Last Invoice (if any)
router.get('/reporting', CustomerController.reporting);

module.exports = router;