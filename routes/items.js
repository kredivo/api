const express = require('express');
const router = express.Router();
const ItemController = require('../controllers/ItemController');

// Get All
router.get('/', ItemController.all);

module.exports = router;