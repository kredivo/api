'use strict';
module.exports = (sequelize, DataTypes) => {
  const items = sequelize.define('items', {
    name: DataTypes.STRING,
    unit_price: DataTypes.DECIMAL
  }, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
  });
  items.associate = function(models) {
    // associations can be defined here
  };
  return items;
};