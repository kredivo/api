'use strict';
module.exports = (sequelize, DataTypes) => {
  const customers = sequelize.define('customers', {
    name: DataTypes.STRING,
    address: DataTypes.TEXT,
    phone: DataTypes.STRING
  }, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
  });
  customers.associate = function(models) {
    // associations can be defined here
    customers.hasOne(models.invoices, {
      as: "last_invoice",
      foreignKey: "customer_id",
      order: 'id desc'
    });
  };
  return customers;
};