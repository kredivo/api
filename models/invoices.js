'use strict';
module.exports = (sequelize, DataTypes) => {
  const invoices = sequelize.define('invoices', {
    customer_id: DataTypes.INTEGER,
    customer_name: DataTypes.STRING,
    customer_address: DataTypes.TEXT,
    customer_phone: DataTypes.STRING,
    sub_total: DataTypes.DECIMAL,
    tax: DataTypes.DECIMAL,
    total: DataTypes.DECIMAL
  }, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
  });
  invoices.associate = function(models) {
    // associations can be defined here
    invoices.hasMany(models.invoice_items, {
      as: "items",
      foreignKey: "invoice_id"
    });
    invoices.belongsTo(models.customers, {
      as: "customer",
      foreignKey: "customer_id"
    });
  };
  return invoices;
};