'use strict';
module.exports = (sequelize, DataTypes) => {
  const invoice_items = sequelize.define('invoice_items', {
    invoice_id: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    unit_price: DataTypes.DECIMAL,
    amount: DataTypes.DECIMAL
  }, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
  });
  invoice_items.associate = function(models) {
    // associations can be defined here
  };
  return invoice_items;
};