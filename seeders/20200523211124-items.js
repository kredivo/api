'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Items', [
      {
        name: 'Item Number 1',
        unit_price: 201.25,
      },
      {
        name: 'Item Number 2',
        unit_price: 125.25,
      },
      {
        name: 'Item Number 3',
        unit_price: 85.25,
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   
   return queryInterface.bulkDelete('Items', null, {})
  }
};
