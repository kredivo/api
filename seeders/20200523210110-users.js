'use strict';

const bcrypt = require('bcrypt')
const saltRounds = 10

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Users', [
    {
      email: 'staff@kredivo.com',
      password: bcrypt.hashSync('staff', saltRounds),
      role: 'staff'      
    },
    {
      email: 'lead@kredivo.com',
      password: bcrypt.hashSync('lead', saltRounds),
      role: 'lead'
    },
    {
      email: 'director@kredivo.com',
      password: bcrypt.hashSync('director', saltRounds),
      role: 'director'
    },
  ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */

   return queryInterface.bulkDelete('Users', {})
  }
};
