'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('Customers', [
      {
        name: 'Ervan',
        address: 'Jalan Kebayoran Lama No. 1 Jakarta Barat, 12345',
        phone: '081288881234'
      },
      {
        name: 'Kurnia',
        address: 'Jalan Kebayoran Baru No. 2 Jakarta Barat, 32451',
        phone: '08888121251'
      },
      {
        name: 'Sanjaya',
        address: 'Jalan Gunung Sahari No. 199B Jakarta Utara, 125125',
        phone: '08212717712'
      },
      {
        name: 'Vivi',
        address: 'Jalan Duta Indah No. 199B Jakarta Utara, 125125',
        phone: '081251909999'
      },
      {
        name: 'Angela',
        address: 'Jalan Kawah Putih No. 199B Jakarta Utara, 125125',
        phone: '088125188888'
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */

    return queryInterface.bulkDelete('Customers', {})
  }
};
