'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Invoices', [
    {
      customer_id: 1,
      sub_total: 201.25,
      tax: 20.12,
      total: 221.37,
      customer_name: 'Ervan',
      customer_address: 'Jalan Kebayoran Lama No. 1 Jakarta Barat, 12345',
      customer_phone: '081288881234'
    },
    {
      customer_id: 2,
      sub_total: 125.25,
      tax: 12.52,
      total: 137.77,
      customer_name: 'Kurnia',
      customer_address: 'Jalan Kebayoran Baru No. 1 Jakarta Barat, 341251',
      customer_phone: '081288881234'
    },
    {
      customer_id: 3,
      sub_total: 300.00,
      tax: 30.00,
      total: 330.00,
      customer_name: 'Sanjaya',
      customer_address: 'Jalan Gunung Sahari No. 199B Jakarta Utara, 125125',
      customer_phone: '08271717217'
    }
  ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   
   return queryInterface.bulkDelete('Invoices', null, {})
  }
};
