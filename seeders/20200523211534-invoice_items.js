'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   
    return queryInterface.bulkInsert('Invoice_items', [
      {
        invoice_id: 1,
        item_id: 1,
        name: 'Item Number 1',
        quantity: 1,
        unit_price: 202.12,
        amount: 202.12
      },
      {
        invoice_id: 2,
        item_id: 2,
        name: 'Item Number 2',
        quantity: 1,
        unit_price: 202.12,
        amount: 202.12
      },
      {
        invoice_id: 2,
        item_id: 3,
        name: 'Item Number 3',
        quantity: 1,
        unit_price: 12.12,
        amount: 12.12
      },
      {
        invoice_id: 3,
        item_id: 1,
        name: 'Item Number 1',
        quantity: 2,
        unit_price: 12.12,
        amount: 24.24
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   
   return queryInterface.bulkDelete('Invoice_items', null, {})
  }
};
