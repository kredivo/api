'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('invoices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      customer_id: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'customers',
          key: 'id'
        }
      },
      customer_name: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      customer_address: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      customer_phone: {
        allowNull: false,
        type: Sequelize.STRING(20)
      },
      sub_total: {
        allowNull: false,
        type: Sequelize.DECIMAL(10,2)
      },
      tax: {
        allowNull: false,
        type: Sequelize.DECIMAL(10,2)
      },
      total: {
        allowNull: false,
        type: Sequelize.DECIMAL(10,2)
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('invoices');
  }
};